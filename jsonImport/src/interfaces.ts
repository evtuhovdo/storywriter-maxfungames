export interface IJSONConditionNPCModeExist {
  Relationship?: string;
  Relatives?: string;
  Gender?: string;
  MinAppearance?: string;
  MinIntelligence?: string;
  MinWillpower?: string;
  MaxWealth?: string;
  MinHealth?: string;
  MaxAppearance?: string;
  MaxIntelligence?: string;
  MaxReligiosity?: string;
  MinSympathy?: string;
  MaxWillpower?: string;
  MinGenerosity?: string;
  MaxAge?: string;
  MaxGenerosity?: string;
  MaxHappiness?: string;
  MaxSympathy?: string;
  MaxHealth?: string;
  MinWealth?: string;
  Occupation?: string;
  MinProductivity?: string;
  MinAge?: string;
  MaxProductivity?: string;
  MinReligiosity?: string;
  MinRelationship?: string;
  MinHappiness?: string;
  MaxRelationship?: string;
  AbsoluteOccupation?: string;
}

export interface IJSONConditionNPCModeDoesNotExist {
  Relationship: string;
  Relatives: string;
  Gender: string;
  Mode: 'DoesNotExist';
}

export interface IInputJson {
  Condition: {
    Character: [
      {
        Perks?: string,
        'MaxSkill:Applied'?: string;
        AbsoluteOccupation?: string;
        'MaxSkill:Sports'?: string;
        Gender?: string;
        SexualPreferences?: string;
        MinIntelligence?: string;
        'MinSkill:Math'?: string;
        'MinSkill:Academical'?: string;
        MaxWealth?: string;
        MaxStress?: string;
        Fertility?: string;
        MinHealth?: string;
        'MinSkill:Art'?: string;
        'MinSkill:Natural'?: string;
        'MaxSkill:Natural'?: string;
        'MinSkill:IT'?: string;
        MinStress?: string;
        'MinSkill:Sports'?: string;
        'MinSkill:Applied'?: string;
        'MaxSkill:Math'?: string;
        MaxEndurance?: string;
        MinStrength?: string;
        MaxAgility?: string;
        'MaxSkill:IT'?: string;
        MaxAge?: string;
        'MinSkill:Technical'?: string;
        MaxHappiness?: string;
        MaxIntelligence?: string;
        MaxHealth?: string;
        MaxAttractiveness?: string;
        'MaxSkill:Art'?: string;
        MinAgility?: string;
        MinWealth?: string;
        'MaxSkill:Academical'?: string;
        Occupation?: string | null;
        MinProductivity?: string;
        MinAge?: string;
        MaxProductivity?: string;
        MaxStrength?: string;
        MinAttractiveness?: string;
        MinHappiness?: string;
        MinEndurance?: string;
        'MaxSkill:Technical'?: string;
      }
    ];
    NPC: (IJSONConditionNPCModeExist | IJSONConditionNPCModeDoesNotExist)[];
  };
  Change: ({
    Character: [ {
      Productivity?: string| null;
      Happiness?: string | null;
      Intelligence?: string | null;
      Health?: string | null;
      Appearance?: string| null;
      Stress?: string| null;
      Agility?: string| null;
      Endurance?: string| null;
      Strength?: string | null;
      Wealth?: string| null;
    } ],
    NPC: {
      Productivity?: string;
      Happiness?: string | null;
      Health?: string;
      Sympathy?: string | null;
      Relationship?: string | null;
      Wealth?: string;
    }[]
  } | null)[] ;
  OptionResult: {
    Result: string;
    DiaryEntry: string;
  }[],
  OptionText: {
    Text: string;
  }[];
  Description: {
    TimeCreated: string,
    Author?: string,
    Comment?: string,
  };
  Category: string;
  HeaderColor: string;
  StoryText: string;
  Question: string;
  ActivityCondition: string;
}

export interface ISelectOption {
  value: string,
  label: string,
}

export interface IOptionValue {
  option: string;
  result: string;
  diaryEntry: string;
  npcChanges: INPCChanges[],
  mainCharacterChanges: IMainCharacterChanges,
}

export interface IMinMaxValueMulti {
  label: string;
  value: {
    min?: string | number;
    max?: string | number;
    title?: string;
  }
}

export interface IMinMaxValue {
  min?: string | number;
  max?: string | number;
  title?: string;
}

export interface IMainCharacterMentalParams {
  happiness?: IMinMaxValue,
  intelligence?: IMinMaxValue,
  stress?: IMinMaxValue,
  productivity?: IMinMaxValue,
}

export interface IMainCharacterSexualPreferences {
  straight: boolean;
  bisexual: boolean;
  gay: boolean;
}

export interface IMainCharacterSocialParams {
  attractiveness?: IMinMaxValue,
  sexualPreferences: IMainCharacterSexualPreferences,
  money?: IMinMaxValue,
  wealth?: IMinMaxValue,
}

export enum EnumNPCType {
  EXIST = 'EXIST',
  DOES_NOT_EXIST = 'DOES_NOT_EXIST',
}

interface INPCBase {
  relative: {
    parent: boolean,
    stepParent: boolean,
    sibling: boolean,
    stepSibling: boolean,
    kid: boolean,
    adoptedKid: boolean,
  },
  relationship: {
    friend: boolean,
    enemy: boolean,
    crush: boolean,
    partner: boolean,
    significantOther: boolean,

    lovers: boolean,
    ex: boolean,
    betrothed: boolean,
    spouse: boolean,
  },
  gender: IGender,
}

interface INPCModeExistBasic extends INPCBase {
  relativeOccupation: {
    classmate: boolean,
    colleague: boolean,
    higherInPosition: boolean,
  }
  absoluteOccupation?: string[],
  age: IMinMaxValueMulti[],
}

export interface INPCModeExist {
  type: EnumNPCType.EXIST,
  basic: INPCModeExistBasic,
  advanced: {
    relationship?: IMinMaxValue,
    passion?: IMinMaxValue,
    happiness?: IMinMaxValue,
    intelligence?: IMinMaxValue,
    health?: IMinMaxValue,
    appearance?: IMinMaxValue,
    productivity?: IMinMaxValue,
    willpower?: IMinMaxValue,
    religiosity?: IMinMaxValue,
    generosity?: IMinMaxValue,
    money?: IMinMaxValue,
  }
}

export interface INPCModeDoesNotExist {
  type: EnumNPCType.DOES_NOT_EXIST,
  basic: INPCBase,
}

export interface IGender {
  male: boolean,
  female: boolean,
  else: boolean,
}

export interface INPCChanges {
  relationship: number,
  passion: number,
  happiness: number,
  health: number,
  productivity: number,
  wealth: string,
}

export interface IMainCharacterPhysicalParams {
  age: IMinMaxValueMulti[],
  health?: IMinMaxValue,
  gender: IGender,
}

export interface IMainCharacterOccupation {
  primarySchool: boolean,
  highSchool: boolean,
  partTimeJob: boolean,
  retirement: boolean,
  middleSchool: boolean,
  college: boolean,
  fullTimeJob: boolean,
  jobless: boolean,
}


export interface IMainCharacter {
  skill?: string[],
  occupation: IMainCharacterOccupation,
  absoluteOccupation?: string[],
  actionReference?: ISelectOption,
  chainStory?: ISelectOption,
  perks?: ISelectOption[],
  flag?: ISelectOption,
  params: {
    physical: IMainCharacterPhysicalParams
    mental: IMainCharacterMentalParams,
    social: IMainCharacterSocialParams,
  },
}

export interface IMainCharacterChanges {
  health: number,
  happiness: number,
  intelligence: number,
  stress: number,
  productivity: number,
  appearance: number,
  wealth: string,
  karma: number,
  flagChange?: ISelectOption,
}

interface ISelectOptionWithColor extends ISelectOption {
  color: string,
}

export interface Values {
  comment?: string,
  storyText: string,
  question: string,
  headerColor?: ISelectOptionWithColor,
  category?: ISelectOption,
  mainCharacter: IMainCharacter,
  npc: (INPCModeExist | INPCModeDoesNotExist)[],
  options: IOptionValue[],
}


export const initialMainCharacter: IMainCharacter = {
  occupation: {
    primarySchool: false,
    highSchool: false,
    partTimeJob: false,
    retirement: false,
    middleSchool: false,
    college: false,
    fullTimeJob: false,
    jobless: false,
  },
  params: {
    mental: {},
    physical: {
      age: [],
      gender: {
        male: false,
        female: false,
        else: false,
      },
    },
    social: {
      sexualPreferences: {
        straight: false,
        bisexual: false,
        gay: false,
      },
    },
  },
};


export const initialOption: IOptionValue = {
  option: '',
  result: '',
  diaryEntry: '',
  npcChanges: [],
  mainCharacterChanges: {
    health: 0,
    karma: 0,
    happiness: 0,
    intelligence: 0,
    stress: 0,
    productivity: 0,
    appearance: 0,
    wealth: '',
  },
};

export const initialValuesEmptyForm: Values = {
  storyText: '',
  question: '',
  options: [
    initialOption,
    initialOption,
  ],
  mainCharacter: initialMainCharacter,
  npc: [ ],
};
