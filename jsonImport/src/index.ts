import * as yup from 'yup';
import shallowequal from 'shallowequal';
import { diff } from 'deep-object-diff';
import * as fs from 'fs';
import * as _ from 'lodash';
import * as collections from './data';
import {
  EnumNPCType,
  IInputJson, IJSONConditionNPCModeExist,
  IMinMaxValue,
  IMinMaxValueMulti,
  initialValuesEmptyForm,
  INPCModeExist,
  IOptionValue,
  ISelectOption,
  Values,
} from './interfaces';
import getDbConnection from './getDbConnection';
import {
  changesAppearance,
  changesHappiness,
  changesHealth,
  changesIntelligence,
  changesProductivity,
  changesStress,
  npcChangesPassion,
  npcChangesRelationship,
} from './data';
// @ts-ignore
import mapFormValuesToJson from '../../frontend/src/helpres/mapFormValuesToJson';
import { ObjectId } from 'mongodb';
import { cloneDeep } from 'lodash';

const dateAndTime = require('date-and-time');

const NPCModeDoesNotExistSchema = yup.object().shape({
  Relationship: yup.string().strict(),
  Relatives: yup.string().strict(),
  Gender: yup.string().strict(),
  Mode: yup.string().oneOf([ 'DoesNotExist' ]).strict(),
});

const NPCModeExistSchema = yup.object().shape({
  Relationship: yup.string().strict(),
  Relatives: yup.string().strict(),
  Gender: yup.string().strict(),
  MinAppearance: yup.string().strict(),
  MinIntelligence: yup.string().strict(),
  MinWillpower: yup.string().strict(),
  MaxWealth: yup.string().strict(),
  MinHealth: yup.string().strict(),
  MaxAppearance: yup.string().strict(),
  MaxRIntelligence: yup.string().strict(),
  MaxReligiosity: yup.string().strict(),
  MinSympathy: yup.string().strict(),
  MaxWillpower: yup.string().strict(),
  MinGenerosity: yup.string().strict(),
  MaxAge: yup.string().strict(),
  MaxGenerosity: yup.string().strict(),
  MaxHappiness: yup.string().strict(),
  MaxSympathy: yup.string().strict(),
  MaxHealth: yup.string().strict(),
  MinWealth: yup.string().strict(),
  Occupation: yup.string().strict(),
  MinProductivity: yup.string().strict(),
  MinAge: yup.string().strict(),
  MaxProductivity: yup.string().strict(),
  MinReligiosity: yup.string().strict(),
  MinRelationship: yup.string().strict(),
  MinHappiness: yup.string().strict(),
  MaxRelationship: yup.string().strict(),
  AbsoluteOccupation: yup.string().strict(),
});

const NPCConditionItemSchema = yup.lazy(value => {
  if (value.Mode === 'DoesNotExist') {
    return NPCModeDoesNotExistSchema;
  }

  return NPCModeExistSchema;
});

const inputJsonSchema = yup.object().shape({
  OptionText: yup.array().of(yup.object({
    Text: yup.string().strict(),
  })),
  Description: yup.object({
    TimeCreated: yup.string().strict(),
    Author: yup.string().strict(),
    Comment: yup.string().strict(),
  }),
  Category: yup.string().strict().default(''),
  HeaderColor: yup.string().strict().default(''),
  StoryText: yup.string().strict(),
  Question: yup.string().strict(),
  ActivityCondition: yup.string().strict().default(''),
  OptionResult: yup.array().of(yup.object({
    Result: yup.string().strict(),
    DiaryEntry: yup.string().strict(),
  })),
  Change: yup.array().of(
    yup.object({
      Character: yup.array().of(
        yup.object({
          Result: yup.string().strict().nullable(),
          Productivity: yup.string().strict().nullable(),
          Happiness: yup.string().strict().nullable(),
          Intelligence: yup.string().strict().nullable(),
          Health: yup.string().strict().nullable().nullable(),
          Appearance: yup.string().strict().nullable(),
          Stress: yup.string().strict().nullable(),
          Agility: yup.string().strict().nullable(),
          Endurance: yup.string().strict().nullable(),
          Strength: yup.string().strict().nullable(),
          Wealth: yup.string().strict().nullable(),
        }),
      )
        .default([]),
      // .test({
      //   message: 'Change.Character.array length must be 1',
      //   test: (arr: any) => {
      //     console.log('arr', arr);
      //     if (typeof arr === 'undefined') {
      //       return true;
      //     }
      //
      //     return Array.isArray(arr) && arr.length === 1;
      //   },
      // }),
      NPC: yup.array().of(yup.object({
        Productivity: yup.string().strict(),
        Happiness: yup.string().strict().nullable(),
        Health: yup.string().strict(),
        Sympathy: yup.string().strict().nullable(),
        Relationship: yup.string().strict().nullable(),
        Wealth: yup.string().strict(),
      })),
    }).nullable(),
  ),

  Condition:
    yup.object({
      Character: yup.array().of(yup.object({
        Perks: yup.string().strict(),
        'MaxSkill:Applied': yup.string().strict(),
        AbsoluteOccupation: yup.string().strict(),
        'MaxSkill:Sports': yup.string().strict(),
        Gender: yup.string().strict(),
        SexualPreferences: yup.string().strict(),
        MinIntelligence: yup.string().strict(),
        'MinSkill:Math': yup.string().strict(),
        'MinSkill:Academical': yup.string().strict(),
        MaxWealth: yup.string().strict(),
        MaxStress: yup.string().strict(),
        Fertility: yup.string().strict(),
        MinHealth: yup.string().strict(),
        'MinSkill:Art': yup.string().strict(),
        'MinSkill:Natural': yup.string().strict(),
        'MaxSkill:Natural': yup.string().strict(),
        'MinSkill:IT': yup.string().strict(),
        MinStress: yup.string().strict(),
        'MinSkill:Sports': yup.string().strict(),
        'MinSkill:Applied': yup.string().strict(),
        'MaxSkill:Math': yup.string().strict(),
        MaxEndurance: yup.string().strict(),
        MinStrength: yup.string().strict(),
        MaxAgility: yup.string().strict(),
        'MaxSkill:IT': yup.string().strict(),
        MaxAge: yup.string().strict(),
        'MinSkill:Technical': yup.string().strict(),
        MaxHappiness: yup.string().strict(),
        MaxIntelligence: yup.string().strict(),
        MaxHealth: yup.string().strict(),
        MaxAttractiveness: yup.string().strict(),
        'MaxSkill:Art': yup.string().strict(),
        MinAgility: yup.string().strict(),
        MinWealth: yup.string().strict(),
        'MaxSkill:Academical': yup.string().strict(),
        Occupation: yup.string().strict().nullable(),
        MinProductivity: yup.string().strict(),
        MinAge: yup.string().strict(),
        MaxProductivity: yup.string().strict(),
        MaxStrength: yup.string().strict(),
        MinAttractiveness: yup.string().strict(),
        MinHappiness: yup.string().strict(),
        MinEndurance: yup.string().strict(),
        'MaxSkill:Technical': yup.string().strict(),
      })).test({
        message: 'Condition.Character.array length must be 1',
        test: (arr: any[]) => Array.isArray(arr) && arr.length === 1,
      }),
      // @ts-ignore
      NPC: yup.array().of(NPCConditionItemSchema).default([]),
    }).default({
      NPC: [],
      Character: [],
    }),
});

// const input: IInputJson = {
//   OptionText: [
//     { Text: '111' },
//     { Text: '222' },
//     { Text: '333' },
//   ],
//   OptionResult: [
//     {
//       Result: 'eqeqwe',
//       DiaryEntry: 'eqeqwe',
//     },
//     {
//       Result: 'eqeqwe',
//       DiaryEntry: 'eqeqwe',
//     },
//     {
//       Result: 'eqeqwe',
//       DiaryEntry: 'eqeqwe',
//     },
//   ],
//   Description: {
//     TimeCreated: 'zzz',
//     Author: 'qqq',
//     Comment: 'zzz',
//   },
//   Category: 'QQQ',
//   HeaderColor: 'ZZZ',
//   StoryText: 'StoryText',
//   Question: 'Question',
//   ActivityCondition: 'ActivityCondition',
//   Change: [
//     {
//       Character: [ {
//         Productivity: 'ValueRef:0:+',
//         Happiness: 'ValueRef:0:+',
//         Intelligence: 'ValueRef:0:+',
//         'Skill:Technical': 'ValueRef:0:+',
//         Health: 'ValueRef:0:+',
//         'Skill:Natural': 'ValueRef:0:+',
//         'Skill:Applied': 'ValueRef:0:+',
//         Appearance: 'ValueRef:0:+',
//         Stress: 'ValueRef:0:+',
//         'Skill:Art': 'ValueRef:0:+',
//         'Skill:Academical': 'ValueRef:0:+',
//         'Skill:Math': 'ValueRef:0:+',
//         Agility: 'ValueRef:0:+',
//         'Skill:IT': 'ValueRef:0:+',
//         Endurance: 'ValueRef:0:+',
//         Strength: 'ValueRef:0:+',
//         'Skill:Sports': 'ValueRef:0:+',
//         Wealth: 'ValueRef:0:+',
//       } ],
//       NPC: [
//         {
//           Productivity: 'ValueRef:0:+',
//           Happiness: 'ValueRef:0:+',
//           Health: 'ValueRef:0:+',
//           Sympathy: 'ValueRef:0:+',
//           Relationship: 'ValueRef:0:+',
//           Wealth: 'ValueRef:0:+',
//         },
//         {
//           Productivity: 'ValueRef:0:+',
//           Happiness: 'ValueRef:0:+',
//           Health: 'ValueRef:0:+',
//           Sympathy: 'ValueRef:0:+',
//           Relationship: 'ValueRef:0:+',
//           Wealth: 'ValueRef:0:+',
//         },
//         {
//           Productivity: 'ValueRef:0:+',
//           Happiness: 'ValueRef:0:+',
//           Health: 'ValueRef:0:+',
//           Sympathy: 'ValueRef:0:+',
//           Relationship: 'ValueRef:0:+',
//           Wealth: 'ValueRef:0:+',
//         },
//       ],
//     },
//   ],
//   Condition: {
//     Character: [
//       {
//         Perks: '132',
//         'MaxSkill:Applied': '132',
//         AbsoluteOccupation: '132',
//         'MaxSkill:Sports': '132',
//         Gender: '132',
//         SexualPreferences: '132',
//         MinIntelligence: '132',
//         'MinSkill:Math': '132',
//         'MinSkill:Academical': '132',
//         MaxWealth: '132',
//         MaxStress: '132',
//         Fertility: '132',
//         MinHealth: '132',
//         'MinSkill:Art': '132',
//         'MinSkill:Natural': '132',
//         'MaxSkill:Natural': '132',
//         'MinSkill:IT': '132',
//         MinStress: '132',
//         'MinSkill:Sports': '132',
//         'MinSkill:Applied': '132',
//         'MaxSkill:Math': '132',
//         MaxEndurance: '132',
//         MinStrength: '132',
//         MaxAgility: '132',
//         'MaxSkill:IT': '132',
//         MaxAge: '132',
//         'MinSkill:Technical': '132',
//         MaxHappiness: '132',
//         MaxIntelligence: '132',
//         MaxHealth: '132',
//         MaxAttractiveness: '132',
//         'MaxSkill:Art': '132',
//         MinAgility: '132',
//         MinWealth: '132',
//         'MaxSkill:Academical': '132',
//         Occupation: '132',
//         MinProductivity: '132',
//         MinAge: '132',
//         MaxProductivity: '132',
//         MaxStrength: '132',
//         MinAttractiveness: '132',
//         MinHappiness: '132',
//         MinEndurance: '132',
//         'MaxSkill:Technical': '132',
//       },
//     ],
//     NPC: [
//       {
//         MinHappiness: "10",
//         Gender: 'Female',
//       },
//       {
//         Gender: 'Male',
//         Mode: 'DoesNotExist',
//       }
//     ],
//   },
// };


// @ts-ignore
const validate = (filename: string) => {
  const rawdata = fs.readFileSync(filename);

  // @ts-ignore
  let json: any;
  try {
    // @ts-ignore
    json = JSON.parse(rawdata);
  } catch (error) {
    console.log('json parse error filaname', filename);
    return {
      filaname: filename,
      valid: false,
    };
  }

  return inputJsonSchema.validate(json, { stripUnknown: true })
    .then((data: any) => {
      const isSame = shallowequal(data, json);
      // console.log('isSame', isSame);
      // @ts-ignore
      // console.log(`${filaname} - valid`, isSame);
      let objDiff;
      if (!isSame) {
        objDiff = diff(json, data);
        // console.log('objDiff', JSON.stringify(objDiff, null, 2));
      }

      return {
        filaname: filename,
        valid: true,
        objDiff,
      };
    })
    .catch((error: any) => {
      console.error(
        `${filename} - error`,
        JSON.stringify(error.value.Change, null, 2),
        error.path,
        error.errors,
      );
      throw new Error('stop');

      return {
        filaname: filename,
        valid: false,
        error,
      };
    });
};


// @ts-ignore
const parseDir = async (dir: string) => {
  const files = fs.readdirSync(`${__dirname}/jsonFiles/source/${dir}/`);
  const jsonFiles = files
    .filter((file) => file.indexOf('.json') !== -1)
    .map((file) => `${__dirname}/jsonFiles/source/${dir}/${file}`);

  try {
    fs.mkdirSync(`${__dirname}/jsonFiles/valid/${dir}`);
    fs.mkdirSync(`${__dirname}/jsonFiles/invalid/${dir}`);
  } catch (e) {

  }

  const results: any[] = [];
  for (let i = 0; i < jsonFiles.length; i++) {
    const file = jsonFiles[i];
    const res = await validate(file);
    results.push(res);

    const pathParts = file.split('/');
    const filename = pathParts[pathParts.length - 1];

    if (res.valid) {
      fs.copyFileSync(file, `${__dirname}/jsonFiles/valid/${dir}/${filename}`);
    } else {
      fs.copyFileSync(file, `${__dirname}/jsonFiles/invalid/${dir}/${filename}`);
    }
  }

  console.log('dir', dir);
  console.log('files count', jsonFiles.length);
  console.log('valid count', results.filter(item => item.valid).length);
  console.log('error count', results.filter(item => !item.valid).length);
};

const doMagic = async () => {
  const { db, client } = await getDbConnection();

  let createdStoryCount = 0;
  let storyWithExidedLimits = new Set<string>();
  let doublesStoryCount = 0;

  const getActionReference = async (value: string): Promise<ISelectOption> => {
    let item = await db
      .collection('action_references')
      .findOne({ title: value });

    if (!item) {
      const newItem = await db
        .collection('action_references')
        .insertOne({
          fromImport: true,
          title: value,
          createdAt: new Date(),
        });

      item = await db
        .collection('action_references')
        .findOne({ _id: newItem.insertedId });
    }

    return {
      value: item._id,
      label: item.title,
    };
  };

  const getMinMax = (min: string | null | undefined, max: string | null | undefined, collection: any): IMinMaxValue | undefined => {
    if (!min && !max) {
      return undefined;
    }

    const search = {};
    if (min) {
      // @ts-ignore
      search.min = parseInt(min);
    }
    if (max) {
      // @ts-ignore
      search.max = parseInt(max);
    }

    const item = _.find(collection, search);

    if (item) {
      return item.value;
    }

    return {
      // @ts-ignore
      title: `${typeof search.min !== 'undefined' ? `min ${search.min}` : ''} ${typeof search.max !== 'undefined' ? `max ${search.max}` : ''}`,
      ...search,
    };
  };

  const getAgeCondition = (min: string | null | undefined, max: string | null | undefined): IMinMaxValueMulti[] => {
    if (!min && !max) {
      return [];
    }

    const search = {};
    if (min) {
      // @ts-ignore
      search.min = parseInt(min);
    }
    if (max) {
      // @ts-ignore
      search.max = parseInt(max);
    }

    const item = _.find(collections.ages, search);

    if (item) {
      // @ts-ignore
      return [ item ];
    }

    // @ts-ignore
    const title = `${typeof search.min !== 'undefined' ? `min ${search.min}` : ''} ${typeof search.max !== 'undefined' ? `max ${search.max}` : ''}`;

    return [ {
      label: title,
      value: {
        ...search,
        title: title,
      },
    } ];
  };
  const getOccupation = (value?: string | null) => {
    const res = {
      primarySchool: false,
      highSchool: false,
      partTimeJob: false,
      retirement: false,
      middleSchool: false,
      college: false,
      fullTimeJob: false,
      jobless: false,
    };

    if (!value) {
      return res;
    }

    const parts = value.split(':');

    if (parts.indexOf('PrimarySchool') !== -1) {
      res.primarySchool = true;
    }
    if (parts.indexOf('MiddleSchool') !== -1) {
      res.middleSchool = true;
    }
    if (parts.indexOf('HighSchool') !== -1) {
      res.highSchool = true;
    }
    if (parts.indexOf('College') !== -1) {
      res.college = true;
    }
    if (parts.indexOf('PartTimeJob') !== -1) {
      res.partTimeJob = true;
    }
    if (parts.indexOf('FullTimeJob') !== -1) {
      res.fullTimeJob = true;
    }
    if (parts.indexOf('Retirement') !== -1) {
      res.retirement = true;
    }
    if (parts.indexOf('Jobless') !== -1) {
      res.jobless = true;
    }

    return res;
  };
  const getGender = (value?: string | null) => {
    const res = {
      male: false,
      female: false,
      else: false,
    };

    if (!value) {
      return res;
    }

    const parts = value.split(':');

    if (parts.indexOf('Male') !== -1) {
      res.male = true;
    }
    if (parts.indexOf('Female') !== -1) {
      res.female = true;
    }
    if (parts.indexOf('Else') !== -1) {
      res.else = true;
    }

    return res;
  };
  const getNpcRelative = (value?: string | null) => {
    const res = {
      parent: false,
      stepParent: false,
      sibling: false,
      stepSibling: false,
      kid: false,
      adoptedKid: false,
    };

    if (!value) {
      return res;
    }

    const parts = value.split(':');

    if (parts.indexOf('Parent') !== -1) {
      res.parent = true;
    }
    if (parts.indexOf('StepParent') !== -1) {
      res.stepParent = true;
    }
    if (parts.indexOf('Sibling') !== -1) {
      res.sibling = true;
    }
    if (parts.indexOf('StepSibling') !== -1) {
      res.stepSibling = true;
    }
    if (parts.indexOf('Kid') !== -1) {
      res.kid = true;
    }
    if (parts.indexOf('AdoptedKid') !== -1) {
      res.adoptedKid = true;
    }

    return res;
  };
  const getNpcReltionship = (value?: string | null) => {
    const res = {
      friend: false,
      enemy: false,
      crush: false,
      partner: false,
      significantOther: false,

      lovers: false,
      ex: false,
      betrothed: false,
      spouse: false,
    };

    if (!value) {
      return res;
    }

    const parts = value.split(':');

    if (parts.indexOf('Friend') !== -1) {
      res.friend = true;
    }
    if (parts.indexOf('Enemy') !== -1) {
      res.enemy = true;
    }
    if (parts.indexOf('Crush') !== -1) {
      res.crush = true;
    }
    if (parts.indexOf('SignificantOther') !== -1) {
      res.significantOther = true;
    }
    if (parts.indexOf('Partner') !== -1) {
      res.partner = true;
    }
    if (parts.indexOf('Engaged') !== -1) {
      res.betrothed = true;
    }
    if (parts.indexOf('Lovers') !== -1) {
      res.lovers = true;
    }
    if (parts.indexOf('Ex') !== -1) {
      res.ex = true;
    }
    if (parts.indexOf('Spouse') !== -1) {
      res.spouse = true;
    }

    return res;
  };
  const getNpcOcupation = (value?: string | null) => {
    const res = {
      classmate: false,
      colleague: false,
      higherInPosition: false,
    };

    if (!value) {
      return res;
    }

    const parts = value.split(':');

    if (parts.indexOf('Classmate') !== -1) {
      res.classmate = true;
    }
    if (parts.indexOf('Colleague') !== -1) {
      res.colleague = true;
    }
    if (parts.indexOf('HigherInPosition') !== -1) {
      res.higherInPosition = true;
    }

    return res;
  };
  const getSexualPreferences = (value?: string | null) => {
    const res = {
      straight: false,
      bisexual: false,
      gay: false,
    };

    if (!value) {
      return res;
    }

    const parts = value.split(':');

    if (parts.indexOf('Straight') !== -1) {
      res.straight = true;
    }
    if (parts.indexOf('Bisexual') !== -1) {
      res.bisexual = true;
    }
    if (parts.indexOf('Gay') !== -1) {
      res.gay = true;
    }

    return res;
  };

  const getExistedStoriesSourceFiles = async () => {
    const existedStories = await db
      .collection('stories')
      .find();

    console.log('existedStories.count()', await existedStories.count());

    const storyIds = new Set<string>();
    await existedStories.forEach((story: any) => {
      if (story.sourceFileName) {
        storyIds.add(String(story.sourceFileName));
      }
    });

    return storyIds;
  };

  const createStoryFromJsonFile = async (file: string) => {
    const rawData = fs.readFileSync(file);
    const values: Values = _.cloneDeep(initialValuesEmptyForm);

    const pathParts = file.split('/');
    const filename = pathParts[pathParts.length - 1];

    const existedStory = await db
      .collection('stories')
      .findOne({
        sourceFileName: filename,
      });

    if (existedStory) {
      console.log(`story ${filename} already exist`);
      doublesStoryCount++;
      return;
    }

    // @ts-ignore
    const sourceJson: IInputJson = JSON.parse(rawData);

    const authorUserName = sourceJson.Description.Author ? String(sourceJson.Description.Author) : 'Unknown';

    let author = await db
      .collection('users-permissions_user')
      .findOne({
        username: authorUserName,
      });

    if (!author) {
      const email = authorUserName.toLowerCase().replace(' ', '_').replace(' ', '_').replace(' ', '_');

      const createdAuthor = await db
        .collection('users-permissions_user')
        .insertOne({
          'confirmed': false,
          'blocked': false,
          'canDoTwoOptions': false,
          'username': authorUserName,
          'email': `${email}@maxfungames.com`,
          'password': '$2a$10$Xwl2wl5e9nQKG5rOMKUJQuSO1E7qINUOCQGpHrs8mf2AX9pB0Krau', // 123456
          'provider': 'local',
          'role': new ObjectId('6149ca3ef1ad8f3af1d2505c'), // Author
        });

      author = await db
        .collection('users-permissions_user')
        .findOne({ _id: createdAuthor.insertedId });
    }

    let authorFinishDate: string | undefined = undefined;
    if (sourceJson.Description.TimeCreated) {
      authorFinishDate = dateAndTime.parse(sourceJson.Description.TimeCreated, 'DD.MM.YYYY H:mm:ss', true);

      if (!authorFinishDate) {
        authorFinishDate = dateAndTime.parse(sourceJson.Description.TimeCreated, 'DD.MM.YYYY HH:mm:ss', true);
      }
    }

    if (!authorFinishDate) {
      authorFinishDate = undefined;
    }

    values.storyText = sourceJson.StoryText;
    values.question = sourceJson.Question;

    // Игнорим категории из json
    //
    // if (sourceJson.Category) {
    //   let category = await db
    //     .collection('story_categories')
    //     .findOne({ title: sourceJson.Category });
    //
    //   if (!category) {
    //     const newCategory = await db
    //       .collection('story_categories')
    //       .insertOne({
    //         fromImport: true,
    //         title: sourceJson.Category,
    //         createdAt: new Date(),
    //       });
    //
    //     category = await db
    //       .collection('story_categories')
    //       .findOne({ _id: newCategory.insertedId });
    //   }
    //
    //   values.category = {
    //     label: sourceJson.Category,
    //     value: category._id,
    //   };
    // }

    if (sourceJson.HeaderColor) {
      let header = await db
        .collection('story_header_colors')
        .findOne({ title: sourceJson.HeaderColor });

      if (!header) {
        const newHeader = await db
          .collection('story_header_colors')
          .insertOne({
            title: sourceJson.HeaderColor,
            createdAt: new Date(),
            value: Math.ceil(Math.random() * 1000000),
          });

        header = await db
          .collection('story_header_colors')
          .findOne({ _id: newHeader.insertedId });
      }

      values.headerColor = {
        label: sourceJson.HeaderColor,
        value: header._id,
        color: header.color,
      };
    }

    if (
      sourceJson.OptionText.length === 4
      && typeof sourceJson.OptionText[3] !== 'undefined'
      && !sourceJson.OptionText[3].Text
    ) {
      sourceJson.OptionText.splice(3, 1);
    }

    if (
      sourceJson.OptionText.length === 3
      && typeof sourceJson.OptionText[2] !== 'undefined'
      && !sourceJson.OptionText[2].Text
    ) {
      sourceJson.OptionText.splice(2, 1);
    }

    const invalidDataComments: string[] = [];

    // if (filename === 'ffb05a50e555b20a.json') {
    //   console.log('values.options before', values.options);
    // }

    for (let i = 0; i < sourceJson.OptionText.length; i++) {
      // if (filename === 'ffb05a50e555b20a.json') {
      //   console.log('iterate i=', i);
      // }
      const option: IOptionValue = {
        option: sourceJson.OptionText[i].Text,
        result: sourceJson.OptionResult[i].Result,
        diaryEntry: sourceJson.OptionResult[i].DiaryEntry,
        npcChanges: [],
        mainCharacterChanges: {
          health: 0,
          happiness: 0,
          intelligence: 0,
          stress: 0,
          productivity: 0,
          appearance: 0,
          wealth: '',
          karma: 0,
        },
      };

      const getValueFromValueRef = (value: string | null | undefined, collection: { value: number }[], fieldName: string): number => {
        if (!value) {
          return 0;
        }

        let index = -1;
        if (collection.length === 8) {
          if (value === 'ValueRef:3:-') {
            // index = 0;
            invalidDataComments.push(`ParseInfo: ${fieldName} value "-100" was ignored`);
            return 0;
          }
          if (value === 'ValueRef:2:-') {
            index = 1;
          }
          if (value === 'ValueRef:1:-') {
            index = 2;
          }
          if (value === 'ValueRef:0:-') {
            index = 3;
          }
          if (value === 'ValueRef:0:+') {
            index = 4;
          }
          if (value === 'ValueRef:1:+') {
            index = 5;
          }
          if (value === 'ValueRef:2:+') {
            index = 6;
          }
          if (value === 'ValueRef:3:+') {
            invalidDataComments.push(`ParseInfo: ${fieldName} value "+100" was ignored`);
            return 0;
            //   index = 7;
          }
        }

        if (collection.length === 6) {
          if (value === 'ValueRef:3:-') {
            index = 0;
          }
          if (value === 'ValueRef:2:-') {
            index = 0;
          }
          if (value === 'ValueRef:1:-') {
            index = 1;
          }
          if (value === 'ValueRef:0:-') {
            index = 2;
          }
          if (value === 'ValueRef:0:+') {
            index = 3;
          }
          if (value === 'ValueRef:1:+') {
            index = 4;
          }
          if (value === 'ValueRef:2:+') {
            index = 5;
          }
          if (value === 'ValueRef:3:+') {
            index = 5;
          }
        }

        if (index !== -1) {
          return collection[index].value;
        }

        return 0;
        // 0 = ValueRef:3:-
        // 1 = ValueRef:2:-
        // 2 = ValueRef:1:-
        // 3 = ValueRef:0:-
        // 4 = ValueRef:0:+
        // 5 = ValueRef:1:+
        // 6 = ValueRef:2:+
        // 7 = ValueRef:3:+


        // 0 = ValueRef:2:-
        // 1 = ValueRef:1:-
        // 2 = ValueRef:0:-
        // 3 = ValueRef:0:+
        // 4 = ValueRef:1:+
        // 5 = ValueRef:2:+
      };

      const changes = sourceJson.Change[i];

      if (changes && changes.Character) {
        const ggChanges = changes.Character[0];

        option.mainCharacterChanges = {
          health: getValueFromValueRef(ggChanges.Health, changesHealth, `option ${i + 1} mainCharacterChanges Health`),
          happiness: getValueFromValueRef(ggChanges.Happiness, changesHappiness, `option ${i + 1} mainCharacterChanges Happiness`),
          intelligence: getValueFromValueRef(ggChanges.Intelligence, changesIntelligence, `option ${i + 1} mainCharacterChanges Intelligence`),
          stress: getValueFromValueRef(ggChanges.Stress, changesStress, `option ${i + 1} mainCharacterChanges Stress`),
          productivity: getValueFromValueRef(ggChanges.Productivity, changesProductivity, `option ${i + 1} mainCharacterChanges Productivity`),
          appearance: getValueFromValueRef(ggChanges.Appearance, changesAppearance, `option ${i + 1} mainCharacterChanges Appearance`),
          wealth: ggChanges.Wealth ? String(ggChanges.Wealth) : '',
          karma: 0,
        };
      }

      if (changes && changes.NPC) {
        for (let j = 0; j < changes.NPC.length; j++) {
          const npcChanges = changes.NPC[i];

          if (!npcChanges) {
            option.npcChanges[j] = {
              wealth: '',
              relationship: 0,
              happiness: 0,
              passion: 0,
              health: 0,
              productivity: 0,
            };
          } else {
            option.npcChanges[j] = {
              wealth: npcChanges.Wealth ? String(npcChanges.Wealth) : '',
              relationship: getValueFromValueRef(npcChanges.Health, npcChangesRelationship, `option ${i + 1} npcChanges Relationship`),
              happiness: getValueFromValueRef(npcChanges.Happiness, changesHappiness, `option ${i + 1} npcChanges Happiness`),
              passion: getValueFromValueRef(npcChanges.Sympathy, npcChangesPassion, `option ${i + 1} npcChanges Passion`),
              health: getValueFromValueRef(npcChanges.Health, changesHealth, `option ${i + 1} npcChanges Health`),
              productivity: getValueFromValueRef(npcChanges.Productivity, changesProductivity, `option ${i + 1} npcChanges Productivity`),
            };
          }
        }
      }

      values.options[i] = option;
    }

    if (filename === 'ffb05a50e555b20a.json') {
      console.log('values.options after', values.options);
    }

    values.mainCharacter = {
      ...values.mainCharacter,
      occupation: getOccupation(sourceJson.Condition.Character[0].Occupation),
      params: {
        physical: {
          age: getAgeCondition(sourceJson.Condition.Character[0].MinAge, sourceJson.Condition.Character[0].MaxAge),
          health: getMinMax(sourceJson.Condition.Character[0].MinHealth, sourceJson.Condition.Character[0].MaxHealth, collections.healths),
          gender: getGender(sourceJson.Condition.Character[0].Gender),
        },
        mental: {
          happiness: getMinMax(sourceJson.Condition.Character[0].MinHappiness, sourceJson.Condition.Character[0].MaxHappiness, collections.happiness),
          intelligence: getMinMax(sourceJson.Condition.Character[0].MinIntelligence, sourceJson.Condition.Character[0].MaxIntelligence, collections.intelligence),
          stress: getMinMax(sourceJson.Condition.Character[0].MinStress, sourceJson.Condition.Character[0].MaxStress, collections.stress),
          productivity: getMinMax(sourceJson.Condition.Character[0].MinProductivity, sourceJson.Condition.Character[0].MaxProductivity, collections.productivity),
        },
        social: {
          sexualPreferences: getSexualPreferences(sourceJson.Condition.Character[0].SexualPreferences),
          attractiveness: getMinMax(sourceJson.Condition.Character[0].MinAttractiveness, sourceJson.Condition.Character[0].MaxAttractiveness, collections.attractiveness),
          wealth: getMinMax(sourceJson.Condition.Character[0].MinWealth, sourceJson.Condition.Character[0].MaxWealth, collections.wealth),
        },
      },
    };

    values.npc = _.map(sourceJson.Condition.NPC, item => {
      // @ts-ignore
      if (item.Mode === 'DoesNotExist') {
        throw new Error('Not implemented');
      }

      const npc = item as IJSONConditionNPCModeExist;

      const npcToValues: INPCModeExist = {
        type: EnumNPCType.EXIST,
        basic: {
          relativeOccupation: getNpcOcupation(npc.Occupation),
          age: getAgeCondition(npc.MinAge, npc.MaxAge),
          relative: getNpcRelative(npc.Relatives),
          relationship: getNpcReltionship(npc.Relationship),
          gender: getGender(npc.Gender),
        },
        advanced: {
          relationship: getMinMax(npc.MinRelationship, npc.MaxRelationship, collections.advancedNPCConditionsRelationship),
          passion: getMinMax(npc.MinSympathy, npc.MaxSympathy, collections.advancedNPCConditionsSympathy),
          happiness: getMinMax(npc.MinHappiness, npc.MaxHappiness, collections.advancedNPCConditionsHappiness),
          intelligence: getMinMax(npc.MinIntelligence, npc.MaxIntelligence, collections.advancedNPCConditionsIntelligence),
          health: getMinMax(npc.MinHealth, npc.MaxHealth, collections.advancedNPCConditionsHealth),
          appearance: getMinMax(npc.MinAppearance, npc.MaxAppearance, collections.advancedNPCConditionsLooks),
          productivity: getMinMax(npc.MinProductivity, npc.MaxProductivity, collections.advancedNPCConditionsProductivity),
          willpower: getMinMax(npc.MinWillpower, npc.MaxWillpower, collections.advancedNPCConditionsWillpower),
          religiosity: getMinMax(npc.MinReligiosity, npc.MaxReligiosity, collections.advancedNPCConditionsReligiosity),
          generosity: getMinMax(npc.MinGenerosity, npc.MaxGenerosity, collections.advancedNPCConditionsGenerosity),
          money: getMinMax(npc.MinRelationship, npc.MaxRelationship, collections.advancedNPCConditionsMoney),
        },
      };

      const filtered = {};
      Object.keys(npcToValues.advanced).filter(prop => {
        const value = npcToValues.advanced[prop];
        if (value) {
          filtered[prop] = value;
        }
      });

      npcToValues.advanced = filtered;

      return npcToValues;
    });

    if (sourceJson.ActivityCondition) {
      values.mainCharacter.actionReference = await getActionReference(sourceJson.ActivityCondition);
    }

    const story = {
      status: 'WAIT_PROOFREADING_OF_PAID',
      author: author._id,
      createdAt: new Date(),
      sourceFileName: filename,
      authorFinishDate: authorFinishDate,
      sourceFileJson: sourceJson,
      comment: sourceJson.Description.Comment,
      formData: values,
      data: mapFormValuesToJson(values),
    };

    console.log(new Date().getTime(), 'inset new story');
    const newStory = await db
      .collection('stories')
      .insertOne(story);
    console.log(new Date().getTime(), 'new story inserted', newStory.insertedId);

    if (sourceJson.Description.Comment) {
      await db
        .collection('story_comments')
        .insertOne(
          {
            'answers': [],
            'text': sourceJson.Description.Comment,
            'createdAt': new Date(),
            'author': author._id,
            'story': newStory.insertedId,
          },
        );
    }

    if (invalidDataComments.length > 0) {
      storyWithExidedLimits.add(newStory.insertedId);
      for (let i = 0; i < invalidDataComments.length; i++) {
        await db
          .collection('story_comments')
          .insertOne(
            {
              'answers': [],
              'text': invalidDataComments[i],
              'createdAt': new Date(),
              'author': author._id,
              'story': newStory.insertedId,
            },
          );
      }
    }

    createdStoryCount++;
    // console.log('newStory', newStory.insertedId);
    // client.close();
    // throw new Error('stop');
  };

  const removeStories = async (dir: string) => {
    const files = fs.readdirSync(`${__dirname}/jsonFiles/valid/${dir}/`);
    const jsonFiles = files
      .filter((file) => file.indexOf('.json') !== -1)
      .map((file) => `${__dirname}/jsonFiles/valid/${dir}/${file}`);

    for (let i = 0; i < jsonFiles.length; i++) {
      const file = jsonFiles[i];

      const pathParts = file.split('/');
      const filename = pathParts[pathParts.length - 1];

      const res = await db
        .collection('stories')
        .deleteOne({
          sourceFileName: filename,
        });

      console.log(`Delete ${res.deletedCount} stories by sourceFileName = ${filename}`);
    }
  };

  const replaceStories = async (dir: string) => {
    await removeStories(dir);
    const storyIds = await getExistedStoriesSourceFiles();
    await createStories(dir, storyIds);
  };

  const createStories = async (dir: string, storyIds: Set<string>) => {
    const files = fs.readdirSync(`${__dirname}/jsonFiles/valid/${dir}/`);
    const jsonFiles = files
      .filter((file) => file.indexOf('.json') !== -1)
      .map((file) => `${__dirname}/jsonFiles/valid/${dir}/${file}`);

    for (let i = 0; i < jsonFiles.length; i++) {
      const file = jsonFiles[i];

      const pathParts = file.split('/');
      const filename = pathParts[pathParts.length - 1];

      if (storyIds.has(filename)) {
        console.log(`story ${filename} already exist`);
        doublesStoryCount++;
      } else {
        await createStoryFromJsonFile(file);
      }
    }
  };

  // await createStories('1');
  // await createStories('2');
  // await createStories('3');
  // await createStories('ActivityStories');
  // await createStories('RandomStories');
  // await createStories('D1');
  // await createStories('D2');
  // await createStories('D3');
  //
  // await createStories('4');
  // await createStories('5');
  // await createStories('6');
  // await createStories('America');

  // await replaceStories('checked');

  client.close();

  console.log('createdStoryCount', createdStoryCount);
  console.log('doublesStoryCount', doublesStoryCount);
  console.log(JSON.stringify(Array.from(storyWithExidedLimits), null, 2));
};

// Проверяет и форматирует JSON
// parseDir('1'); // архив (13.08 - 10.09)
// parseDir('2'); // Архив из билда до 08.09.2021
// parseDir('3'); // Json
// parseDir('RandomStories'); // Скинул Дима программист
// parseDir('ActivityStories'); // Скинул Дима программист
// parseDir('D1'); // Json-20211019T114654Z-001.zip
// parseDir('D2'); // Json 2.10 - 8.10.zip
// parseDir('D3'); // Json 24 сентября по 1 октября.zip
// parseDir('4'); // Json 2 - 10 октября
// parseDir('5'); // Json 18 - 24 сентября
// parseDir('6'); // Json 24 сентября - 1 октября
// parseDir('America'); // Истории американских авторов
// parseDir('checked'); // Истории американских авторов

const formatStories = async () => {
  const { db, client } = await getDbConnection();

  const unsetAllCategories = async () => {
    const storiesWithStatusDownGrade = [];

    const existedStories = await db
      .collection('stories')
      .find({
        'formData.category': {
          '$exists': true,
        },
      });

    const storiesWithErrors = new Set<string>();

    let i = 0;
    while (await existedStories.hasNext()) {
      const story = await existedStories.next();
      try {
        i++;
        console.log(`${i}/6048) story id = ${story._id}`);
        if (story.formData) {

          const formData = typeof story.formData === 'string' ? JSON.parse(story.formData) : story.formData;

          if (!formData.category) {
            continue;
          }
          delete formData.category;

          const data = mapFormValuesToJson(formData);

          await db
            .collection('stories')
            .updateOne(
              {
                _id: story._id,
              },
              [
                {
                  $set: {
                    data: data,
                    'formData.category': '',
                  },
                },
              ],
              {
                bypassDocumentValidation: true,
              },
            );

          if (story.status === 'APPROVED' || story.status === 'PROOFREADING_OF_PAID_DONE') {
            const newStatus = story.status === 'APPROVED' ? 'WAIT_FOR_APPROVE' : 'WAIT_PROOFREADING_OF_PAID';

            storiesWithStatusDownGrade.push(story._id);
            await db
              .collection('stories')
              .updateOne(
                {
                  _id: story._id,
                },
                [
                  {
                    $set: {
                      status: newStatus,
                    },
                  },
                ],
                {
                  bypassDocumentValidation: true,
                },
              );

            await db
              .collection('story_comments')
              .insertOne(
                {
                  'answers': [],
                  'text': 'Return from Approved status after unset category',
                  'createdAt': new Date(),
                  'author': story.author,
                  'story': story._id,
                },
              );
          }
        }
      } catch (error) {
        console.error(error);
        console.log(`error with story ${story._id}`);
        storiesWithErrors.add(story._id)
      }
    }
    console.log('storiesWithErrors', storiesWithErrors);
    console.log('storiesWithStatusDownGrade', storiesWithStatusDownGrade);
  };

  await unsetAllCategories();
  client.close();
};

// Наполняет БД записями используя результаты прошлого шага
// doMagic();

// Очищает истории от лишних данных
// formatStories();

const fixNPCChangesBulk = async () => {
  const { db, client } = await getDbConnection();

  const fixNPCChangesInStory = async (story: any) => {
    const fixedStory = _.cloneDeep(story);

    const formData = fixedStory.formData;
    const npcCount = formData.npc.length;

    formData.options = _.map(formData.options, (item) => {
      return {
        ...item,
        npcChanges: npcCount > 0 ? item.npcChanges.splice(0, npcCount) : [],
      }
    });

    fixedStory.data = mapFormValuesToJson(formData);

    await db
      .collection('stories')
      .replaceOne(
        {
          _id: story._id,
        },
        fixedStory
      );
  };

  const fixNPCChanges = async () => {
    const existedStories = await db
      .collection('stories')
      .find();

    const storyCount = await existedStories.count();
    let i = 0;
    while (await existedStories.hasNext()) {
      i ++;
      const story = await existedStories.next();
      console.log(`${i} from ${storyCount} - ${story._id}`);
      await fixNPCChangesInStory(story);
    }
  }

  await fixNPCChanges();
  client.close();
};

// fixNPCChangesBulk();
