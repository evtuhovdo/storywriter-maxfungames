export const ages = [
  {
    value: {
      title: '1-2 y/o',
      min: 1,
      max: 2,
    },
    label: '1-2 y/o',
  },
  {
    value: {
      title: '3-4 y/o',
      min: 3,
      max: 4,
    },
    label: '3-4 y/o',
  },
  {
    value: {
      title: '5-9 y/o',
      min: 5,
      max: 9,
    },
    label: '5-9 y/o',
  },
  {
    value: {
      title: '10-13 y/o',
      min: 10,
      max: 13,
    },
    label: '10-13 y/o',
  },
  {
    value: {
      title: '14-17 y/o',
      min: 14,
      max: 17,
    },
    label: '14-17 y/o',
  },
  {
    value: {
      title: '18-30 y/o',
      min: 18,
      max: 30,
    },
    label: '18-30 y/o',
  },
  {
    value: {
      title: '31-65 y/o',
      min: 31,
      max: 65,
    },
    label: '31-65 y/o',
  },
  {
    value: {
      title: '65+ y/o',
      min: 65,
    },
    label: '65+ y/o',
  },
];
export const healths = [
  {
    value: {
      title: 'Dying',
      min: 1,
      max: 25,
    },
    label: 'Dying 1-25',
  },
  {
    value: {
      title: 'Sick',
      min: 26,
      max: 50,
    },
    label: 'Sick 26-50',
  },
  {
    value: {
      title: 'Comfort',
      min: 51,
      max: 75,
    },
    label: 'Comfort 51-75',
  },
  {
    value: {
      title: 'Thriving',
      min: 76,
      max: 100,
    },
    label: 'Thriving 76-100',
  },
];
export const happiness = [
  {
    value: {
      title: 'Disaster',
      min: 1,
      max: 25,
    },
    label: 'Disaster 1-25',
  },
  {
    value: {
      title: 'Discomfort',
      min: 26,
      max: 50,
    },
    label: 'Discomfort 26-50',
  },
  {
    value: {
      title: 'Joy',
      min: 51,
      max: 75,
    },
    label: 'Joy 51-75',
  },
  {
    value: {
      title: 'Delight',
      min: 76,
      max: 100,
    },
    label: 'Delight 76-100',
  },
];
export const intelligence = [
  {
    value: {
      title: 'Imbecile',
      min: 1,
      max: 25,
    },
    label: 'Imbecile 1-25',
  },
  {
    value: {
      title: 'Dumb',
      min: 26,
      max: 50,
    },
    label: 'Dumb 26-50',
  },
  {
    value: {
      title: 'Clever',
      min: 51,
      max: 75,
    },
    label: 'Clever 51-75',
  },
  {
    value: {
      title: 'Genius',
      min: 76,
      max: 100,
    },
    label: 'Genius 76-100',
  },
];
export const stress = [
  {
    value: {
      title: 'Peace',
      min: 1,
      max: 25,
    },
    label: 'Peace 1-25',
  },
  {
    value: {
      title: 'Peace',
      min: 26,
      max: 50,
    },
    label: 'Relief 26-50',
  },
  {
    value: {
      title: 'Pressure',
      min: 51,
      max: 75,
    },
    label: 'Pressure 51-75',
  },
  {
    value: {
      title: 'Anxiety',
      min: 76,
      max: 100,
    },
    label: 'Anxiety 76-100',
  },
];
export const productivity = [
  {
    value: {
      title: 'Useless',
      min: 1,
      max: 25,
    },
    label: 'Useless 1-25',
  },
  {
    value: {
      title: 'Lazy',
      min: 26,
      max: 50,
    },
    label: 'Lazy 26-50',
  },
  {
    value: {
      title: 'Active',
      min: 51,
      max: 75,
    },
    label: 'Active 51-75',
  },
  {
    value: {
      title: 'Productive',
      min: 76,
      max: 100,
    },
    label: 'Productive 76-100',
  },
];
export const attractiveness = [
  {
    value: {
      title: 'Abomination',
      min: 1,
      max: 25,
    },
    label: 'Abomination 1-25',
  },
  {
    value: {
      title: 'Mess',
      min: 26,
      max: 50,
    },
    label: 'Mess 26-50',
  },
  {
    value: {
      title: 'Beautiful',
      min: 51,
      max: 75,
    },
    label: 'Beautiful 51-75',
  },
  {
    value: {
      title: 'Gorgeous',
      min: 76,
      max: 100,
    },
    label: 'Gorgeous 76-100',
  },
];
export const money = [
  {
    value: {
      title: 'Broke',
      min: 0,
      max: 250,
    },
    label: 'Broke 0-250',
  },
  {
    value: {
      title: 'In need',
      min: 251,
      max: 5000,
    },
    label: 'In need 251-5 000',
  },
  {
    value: {
      title: 'Comfortable',
      min: 5001,
      max: 250000,
    },
    label: 'Comfortable 5 001-250 000',
  },
  {
    value: {
      title: 'Wealthy',
      min: 250001,
      max: 999999999999,
    },
    label: 'Wealthy 250 001-999 999 999 999',
  },
];
export const wealth = [
  {
    value: {
      title: 'Poor',
      min: 1,
      max: 15,
    },
    label: 'Poor 1-15',
  },
  {
    value: {
      title: 'In need',
      min: 16,
      max: 30,
    },
    label: 'In need 16-30',
  },
  {
    value: {
      title: 'Comfortable',
      min: 31,
      max: 60,
    },
    label: 'Comfortable 31-60',
  },
  {
    value: {
      title: 'Wealthy',
      min: 61,
      max: 100,
    },
    label: 'Wealthy 61-100',
  },
];

export const changesHealth = [
  {
    value: -100,
    label: 'Death -100',
  },
  {
    value: -20,
    label: 'Even worse -20',
  },
  {
    value: -10,
    label: 'Much worse -10',
  },
  {
    value: -5,
    label: 'Got a little bit worse -5',
  },
  {
    value: 5,
    label: 'Got a little bit better +5',
  },
  {
    value: 10,
    label: 'Much better +10',
  },
  {
    value: 20,
    label: 'Even better +20',
  },
  {
    value: 100,
    label: 'Thriving +100',
  },
];

export const changesKarma = [
  {
    value: 5,
    label: '+5 increased significantly',
  },
  {
    value: 2,
    label: '+2 increased',
  },
  {
    value: -2,
    label: '-2 decreased',
  },
  {
    value: -5,
    label: '-5 decreased significantly',
  },
];

export const changesHappiness = [
  {
    value: -20,
    label: 'Even more sad -20',
  },
  {
    value: -10,
    label: 'Much sadder -10',
  },
  {
    value: -5,
    label: 'Got a little bit sadder -5',
  },
  {
    value: 5,
    label: 'Became a little bit happier +5',
  },
  {
    value: 10,
    label: 'Much happier +10',
  },
  {
    value: 20,
    label: 'Even more happy +20',
  },
];

export const changesIntelligence = [
  {
    value: -20,
    label: 'Even more dumb -20',
  },
  {
    value: -10,
    label: 'Much dumber -10',
  },
  {
    value: -5,
    label: 'Got a little bit dumber -5',
  },
  {
    value: 5,
    label: 'Got a little bit smarter +5',
  },
  {
    value: 10,
    label: 'Much smarter +10',
  },
  {
    value: 20,
    label: 'Even more smart +20',
  },
];

export const changesStress = [
  {
    value: -20,
    label: 'Even more relaxed -20',
  },
  {
    value: -10,
    label: 'Much more relaxed -10',
  },
  {
    value: -5,
    label: 'Got a little bit relaxed -5',
  },
  {
    value: 5,
    label: 'Got a little bit stressed +5',
  },
  {
    value: 10,
    label: 'Much more stressed +10',
  },
  {
    value: 20,
    label: 'Even more stressed +20',
  },
];

export const changesProductivity = [
  {
    value: -20,
    label: 'Even more lazy -20',
  },
  {
    value: -10,
    label: 'Much lazier -10',
  },
  {
    value: -5,
    label: 'Got a little bit lazy -5',
  },
  {
    value: 5,
    label: 'Got a little bit productive +5',
  },
  {
    value: 10,
    label: 'Much more productive +10',
  },
  {
    value: 20,
    label: 'Even more productive +20',
  },
];

export const changesAppearance = [
  {
    value: -20,
    label: 'Even more ugly -20',
  },
  {
    value: -10,
    label: 'Much uglier -10',
  },
  {
    value: -5,
    label: 'Got a little bit uglier -5',
  },
  {
    value: 5,
    label: 'Got a little bit beautiful +5',
  },
  {
    value: 10,
    label: 'Much more beautiful +10',
  },
  {
    value: 20,
    label: 'Even more beautiful +20',
  },
];

export const npcChangesRelationship = [
  {
    value: -100,
    label: 'Became enemies -100',
  },
  {
    value: -20,
    label: 'Even worse -20',
  },
  {
    value: -10,
    label: 'Much worse -10',
  },
  {
    value: -5,
    label: 'Got a little bit worse -5',
  },
  {
    value: 5,
    label: 'Got a little bit better +5',
  },
  {
    value: 10,
    label: 'Much better +10',
  },
  {
    value: 20,
    label: 'Even better +20',
  },
  {
    value: 90,
    label: 'Ride or Die +90',
  },
];

export const npcChangesPassion = [
  {
    value: -20,
    label: 'Even less sympathetic -20',
  },
  {
    value: -10,
    label: 'Much less sympathetic -10',
  },
  {
    value: -5,
    label: 'Got a little bit less sympathetic -5',
  },
  {
    value: 5,
    label: 'Got a little bit more sympathetic +5',
  },
  {
    value: 10,
    label: 'Much more sympathetic +10',
  },
  {
    value: 20,
    label: 'Even more sympathetic +20',
  },
];

export const advancedNPCConditionsRelationship = [
  {
    value: {
      title: 'Enemy',
      min: 0,
      max: 25,
    },
    label: 'Enemy 0-25',
  },
  {
    value: {
      title: 'Disrespect',
      min: 26,
      max: 50,
    },
    label: 'Disrespect 26-50',
  },
  {
    value: {
      title: 'Matey',
      min: 51,
      max: 75,
    },
    label: 'Disrespect 51-75',
  },
  {
    value: {
      title: 'Ride or die',
      min: 76,
    },
    label: 'Ride or die 76+',
  },
];

export const advancedNPCConditionsSympathy = [
  {
    value: {
      title: 'Indifference',
      min: 0,
      max: 25,
    },
    label: 'Indifference 0-25',
  },
  {
    value: {
      title: 'Interest',
      min: 26,
      max: 50,
    },
    label: 'Interest 26-50',
  },
  {
    value: {
      title: 'Affection',
      min: 51,
      max: 75,
    },
    label: 'Affection 51-75',
  },
  {
    value: {
      title: 'Love',
      min: 76,
    },
    label: 'Love 76+',
  },
];

export const advancedNPCConditionsHappiness = [
  {
    value: {
      title: 'Disaster',
      min: 0,
      max: 25,
    },
    label: 'Disaster 0-25',
  },
  {
    value: {
      title: 'Discomfort',
      min: 26,
      max: 50,
    },
    label: 'Discomfort 26-50',
  },
  {
    value: {
      title: 'Joy',
      min: 51,
      max: 75,
    },
    label: 'Joy 51-75',
  },
  {
    value: {
      title: 'Delight',
      min: 76,
    },
    label: 'Delight 76+',
  },
];

export const advancedNPCConditionsIntelligence = [
  {
    value: {
      title: 'Imbecile',
      min: 0,
      max: 25,
    },
    label: 'Imbecile 0-25',
  },
  {
    value: {
      title: 'Dumb',
      min: 26,
      max: 50,
    },
    label: 'Dumb 26-50',
  },
  {
    value: {
      title: 'Clever',
      min: 51,
      max: 75,
    },
    label: 'Clever 51-75',
  },
  {
    value: {
      title: 'Genius',
      min: 76,
    },
    label: 'Genius 76+',
  },
];

export const advancedNPCConditionsHealth = [
  {
    value: {
      title: 'Dying',
      min: 0,
      max: 25,
    },
    label: 'Dying 0-25',
  },
  {
    value: {
      title: 'Sick',
      min: 26,
      max: 50,
    },
    label: 'Sick 26-50',
  },
  {
    value: {
      title: 'Comfort',
      min: 51,
      max: 75,
    },
    label: 'Comfort 51-75',
  },
  {
    value: {
      title: 'Thriving',
      min: 76,
    },
    label: 'Thriving 76+',
  },
];

export const advancedNPCConditionsLooks = [
  {
    value: {
      title: 'Abomination',
      min: 0,
      max: 25,
    },
    label: 'Abomination 0-25',
  },
  {
    value: {
      title: 'Mess',
      min: 26,
      max: 50,
    },
    label: 'Mess 26-50',
  },
  {
    value: {
      title: 'Beautiful',
      min: 51,
      max: 75,
    },
    label: 'Beautiful 51-75',
  },
  {
    value: {
      title: 'Gorgeous',
      min: 76,
    },
    label: 'Gorgeous 76+',
  },
];

export const advancedNPCConditionsProductivity = [
  {
    value: {
      title: 'Useless',
      min: 0,
      max: 25,
    },
    label: 'Useless 0-25',
  },
  {
    value: {
      title: 'Lazy',
      min: 26,
      max: 50,
    },
    label: 'Lazy 26-50',
  },
  {
    value: {
      title: 'Active',
      min: 51,
      max: 75,
    },
    label: 'Active 51-75',
  },
  {
    value: {
      title: 'Productive',
      min: 76,
    },
    label: 'Productive 76+',
  },
];

export const advancedNPCConditionsWillpower = [
  {
    value: {
      title: 'Doormat',
      min: 0,
      max: 25,
    },
    label: 'Doormat 0-25',
  },
  {
    value: {
      title: 'Hesitant',
      min: 26,
      max: 50,
    },
    label: 'Hesitant 26-50',
  },
  {
    value: {
      title: 'Decisive',
      min: 51,
      max: 75,
    },
    label: 'Decisive 51-75',
  },
  {
    value: {
      title: 'Strong-willed',
      min: 76,
    },
    label: 'Strong-willed 76+',
  },
];


export const advancedNPCConditionsReligiosity = [
  {
    value: {
      title: 'Atheist',
      min: 0,
      max: 25,
    },
    label: 'Atheist 0-25',
  },
  {
    value: {
      title: 'Skeptic',
      min: 26,
      max: 50,
    },
    label: 'Skeptic 26-50',
  },
  {
    value: {
      title: 'Religious',
      min: 51,
      max: 75,
    },
    label: 'Religious 51-75',
  },
  {
    value: {
      title: 'Believer',
      min: 76,
    },
    label: 'Believer 76+',
  },
];

export const advancedNPCConditionsGenerosity = [
  {
    value: {
      title: 'Greedy',
      min: 0,
      max: 25,
    },
    label: 'Greedy 0-25',
  },
  {
    value: {
      title: 'Stingy',
      min: 26,
      max: 50,
    },
    label: 'Stingy 26-50',
  },
  {
    value: {
      title: 'Charitable',
      min: 51,
      max: 75,
    },
    label: 'Charitable 51-75',
  },
  {
    value: {
      title: 'Lavish',
      min: 76,
    },
    label: 'Lavish 76+',
  },
];

export const advancedNPCConditionsMoney = [
  {
    value: {
      title: 'Broke',
      min: 0,
      max: 250,
    },
    label: 'Broke 0-250',
  },
  {
    value: {
      title: 'In need',
      min: 251,
      max: 5000,
    },
    label: 'In need 251-5 000',
  },
  {
    value: {
      title: 'Comfortable',
      min: 5001,
      max: 250000,
    },
    label: 'Comfortable 5 001-250 000',
  },
  {
    value: {
      title: 'Wealthy',
      min: 250001,
      max: 999999999999,
    },
    label: 'Wealthy 250 001-999 999 999 999',
  },
];
