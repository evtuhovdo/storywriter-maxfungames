const nameOfNPC = (name: string, i: number): string => {
  return name.replace('npc.', `npc[${i}].`);
};

export default nameOfNPC;
