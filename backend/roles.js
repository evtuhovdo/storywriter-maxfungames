const AUTHOR = 'Author';
const ADMIN = 'Admin';
const EDITOR = 'Editor';

module.exports = {
  AUTHOR,
  ADMIN,
  EDITOR,
}
