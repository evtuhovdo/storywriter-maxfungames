'use strict';

const {reduce} = require('lodash');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */


module.exports = {
  lifecycles: {
    async afterCreate(result) {
      // расчет рейтинга автора
      const records = await strapi.services['rating-record'].find({
        target: result.target.id,
      });

      const ratingSum = reduce(records, (sum, record) => sum + record.score, 0);
      const ratingCount = records.length;
      const rating = (ratingSum / ratingCount).toFixed(2);

      await strapi.query('user', 'users-permissions').update({id: result.target.id}, {rating, ratingCount});

      // расчет рейтинга истории
      const recordsByStory = await strapi.services['rating-record'].find({
        story: result.story.id,
      });

      const ratingStorySum = reduce(recordsByStory, (sum, record) => sum + record.score, 0);
      const ratingStoryCount = recordsByStory.length;
      const ratingStory = (ratingStorySum / ratingStoryCount).toFixed(2);

      await strapi.services.story.update({id: result.story.id}, {calculatedRatingOne: ratingStory});
    },
    async afterUpdate(result) {
      // расчет рейтинга автора
      const records = await strapi.services['rating-record'].find({
        target: result.target.id,
      });

      const ratingSum = reduce(records, (sum, record) => sum + record.score, 0);
      const ratingCount = records.length;
      const rating = (ratingSum / ratingCount).toFixed(2);

      await strapi.query('user', 'users-permissions').update({id: result.target.id}, {rating, ratingCount});

      // расчет рейтинга истории
      const recordsByStory = await strapi.services['rating-record'].find({
        story: result.story.id,
      });

      const ratingStorySum = reduce(recordsByStory, (sum, record) => sum + record.score, 0);
      const ratingStoryCount = recordsByStory.length;
      const ratingStory = (ratingStorySum / ratingStoryCount).toFixed(2);

      await strapi.services.story.update({id: result.story.id}, {calculatedRatingOne: ratingStory});
    },
  }
};
