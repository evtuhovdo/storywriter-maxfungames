'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  async afterCreate(result) {
    await strapi.services.story.update({id: result.id}, {stringId: result.id.toString()});
  },
  async afterUpdate(result) {
    await strapi.services.story.update({id: result.id}, {stringId: result.id.toString()});
  },
};
